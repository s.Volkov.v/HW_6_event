﻿using System;
namespace Homework_DelegateEvents
{
    public static class IEnumerableExtension
    {

        public static T GetTheBiggestObj<T>(this IEnumerable<T> array, Func<T,T,bool> predicate)
        {
            T theBiggest = array.First();
            foreach (T obj in array)
            {
                if (predicate(obj,theBiggest))
                {
                    theBiggest = obj;
                }
            }
            return theBiggest;
        }

    }
}

