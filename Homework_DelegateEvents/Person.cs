﻿using System;
namespace Homework_DelegateEvents
{
    public record Person
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Age { get; set; }
    }
}

